execute as @a store result score @s charge_count run clear @s minecraft:fire_charge{fireorb:1} 0


execute as @e[tag=rotationMotion,tag=!motionAdded] at @s rotated as @p run function carrots:fireball/motion

execute as @e[tag=rotationMotion] run scoreboard players add @s fireball_life 1
execute as @e[tag=rotationMotion,scores={fireball_life=60..}] run kill @s

